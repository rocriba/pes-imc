package cat.dam.rocriba.pesiimc;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class imcActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_imc);
        TextView info1, grup;

        info1 = (TextView) findViewById(R.id.textView4);
        grup = (TextView) findViewById(R.id.textView5);
        final Button btn_2 = (Button) findViewById(R.id.btn_2);
        //Recuperem la info
        Bundle bundle = this.getIntent().getExtras();
        Float IMC = bundle.getFloat("PESR")/(bundle.getFloat("ALTURAR")*2);


        //Fem el text
        info1.setText("Per un pes de: " + bundle.getFloat("PESR") + " i una alçada de: " + bundle.getFloat("ALTURAR") + " el seu IMC es de: " + IMC);

        //Clasificacio IMC
        if (IMC <= 18.5){
            grup.setText("Vosté es troba en el grup: Pes Baix, insuficient");
        } else if (IMC >= 18.5 && IMC <= 24.9){
            grup.setText("Vosté es troba en el grup: Pes Normal");
        } else if (IMC >=25.0 && IMC <= 29.9){
            grup.setText("Vosté es troba en el grup: Sobrepes");
        } else if (IMC >= 30.0 && IMC <= 34.5){
            grup.setText("Vosté es troba en el grup: Obesitat Grau I");
        } else if (IMC >=35.0 && IMC <= 39.9){
            grup.setText("Vosté es troba en el grup: Obesitat Grau II");
        } else if (IMC >= 40.0){
            grup.setText("Vosté es troba en el grup: Obesitat Grau III");
        } else {
            grup.setText("Vosté es troba en el grup: Error IMC");
        }

        //Fem el boto perquè quan el cliquin tornem al layout principal
        btn_2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent_tornar = new Intent(imcActivity.this, MainActivity.class);
                startActivity(intent_tornar);


            }
        });
    }
}
