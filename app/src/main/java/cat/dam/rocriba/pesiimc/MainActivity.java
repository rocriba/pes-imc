package cat.dam.rocriba.pesiimc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {

     EditText et_kg, et_altura;
    private Button btn_1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_kg = (EditText) findViewById(R.id.et_1);
        et_altura = (EditText) findViewById(R.id.et_2);
        btn_1 = (Button) findViewById(R.id.btn_1);


        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //Agafem la info del edit Text

                String string_kg = et_kg.getText().toString();
                String string_altura = et_altura.getText().toString();

                Float kg, altura;
                kg = Float.parseFloat(string_kg);
                altura = Float.parseFloat(string_altura);

                //Creem un intent, per poder passar la info
                Intent I = new Intent(MainActivity.this, imcActivity.class);
                //Creem el bundle per pasar la info entre activitats
                Bundle b = new Bundle();

              //  b.putFloat("PESR", kg);
              //  b.putFloat("ALTURAR", altura);
                I.putExtra("PESR", kg);
                I.putExtra("ALTURAR", altura);


                //inicar activity
               startActivity(I);
            }
        });


    }
}